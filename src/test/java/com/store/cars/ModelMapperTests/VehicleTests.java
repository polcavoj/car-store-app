package com.store.cars.ModelMapperTests;

import com.store.cars.Gear.Gear;
import com.store.cars.Brand.Brand;
import com.store.cars.Vehicle.DTO.VehicleDTO;
import com.store.cars.Vehicle.Vehicle;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VehicleTests {
    private static final ModelMapper modelMapper = new ModelMapper();

    @Test
    public void checkVehicleMapping(){
        Brand brand = new Brand();
        brand.setId(0);
        brand.setName("Škoda");

        Gear gear = new Gear();
        gear.setId(0);
        gear.setName("test");
        List<Gear> gears = new ArrayList<>();
        gears.add(gear);

        VehicleDTO vehicleDTO = new VehicleDTO();
        vehicleDTO.setCategory("Automobil");
        vehicleDTO.setBrand_id(brand);
        vehicleDTO.setModel("Octavia");
        vehicleDTO.setVehicleType("Laurin a Klement");
        vehicleDTO.setPrice(300000);
        vehicleDTO.setCommissioning(LocalDate.of(2019,04,04));
        vehicleDTO.setGears(gears);

        Vehicle vehicle = modelMapper.map(vehicleDTO, Vehicle.class);
        assertEquals(vehicleDTO.getCategory(), vehicle.getCategory());
        assertEquals(vehicleDTO.getBrand_id(), vehicle.getBrand_id());
        assertEquals(vehicleDTO.getModel(), vehicle.getModel());
        assertEquals(vehicleDTO.getVehicleType(), vehicle.getVehicleType());
        assertEquals(vehicleDTO.getPrice(), vehicle.getPrice());
        assertEquals(vehicleDTO.getCommissioning(), vehicle.getCommissioning());
        assertEquals(vehicleDTO.getGears(), vehicle.getGears());
    }
}
