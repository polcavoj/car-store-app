package com.store.cars.Vehicle;

import com.store.cars.Brand.Brand;
import com.store.cars.Gear.Gear;
import com.store.cars.System.LocalDateAttributeConverter;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.core.style.ToStringCreator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple Java class which represent Vehicle entity.
 */
@Entity
@Table(name = "vehicles")
public class Vehicle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer     id;

    @Column(name = "category")
    @NotEmpty
    private String      category;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand_id;

    @Column(name = "model")
    @NotEmpty
    private String      model;

    @Column(name = "vehicle_type")
    @NotEmpty
    private String      vehicleType;

    @Column(name = "price")
    @NotNull
    private Integer     price;

    @Column(name = "commissioning")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate   commissioning;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "gear_in_vehicles", joinColumns = @JoinColumn(name = "vehicle_id"), inverseJoinColumns = @JoinColumn(name = "gear_id"))
    private List<Gear>   gears;

    @Column(name = "sold_at_date")
    @DateTimeFormat(pattern = "yyyy-MM-DD")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate   soldAtDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() { return category; }

    public void setCategory(String category) { this.category = category;}

    public Brand getBrand_id() { return brand_id; }

    public void setBrand_id(Brand brand_id) { this.brand_id = brand_id; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public String getVehicleType() { return vehicleType; }

    public void setVehicleType(String vehicleType) { this.vehicleType = vehicleType; }

    public Integer getPrice() { return price; }

    public void setPrice(Integer price) { this.price = price; }

    public LocalDate getCommissioning() { return commissioning; }

    public void setCommissioning(LocalDate commissioning) { this.commissioning = commissioning; }

    public LocalDate getSoldAtDate() {
        return soldAtDate;
    }

    public void setSoldAtDate(LocalDate soldAtDate) {
        this.soldAtDate = soldAtDate;
    }

    public boolean isNew() {
        return this.id == null;
    }

    public List<Gear> getGearsInternal(){
        return gears;
    }

    /**
     * Sorting method for a list of gear in a vehicle.
     * @return sorted gear of a vehicle
     */
    public List<Gear> getGears() {
        List<Gear> sortedGear = new ArrayList<>(getGearsInternal());
        PropertyComparator.sort(sortedGear, new MutableSortDefinition("id", true, true));
        return Collections.unmodifiableList(sortedGear);
    }

    public void setGears(List<Gear> gears) {
        this.gears = gears;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id ", this.getId()).append("new", this.isNew())
                .append("category ", this.getCategory())
                .append("brand ", this.getBrand_id())
                .append("model ", this.getModel())
                .append("type ", this.getVehicleType())
                .append("price ", this.getPrice())
                .append("commissioning ", this.getCommissioning())
                .append("soldAtDate ", this.getSoldAtDate()).toString();
    }
}
