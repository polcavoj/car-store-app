package com.store.cars.Vehicle;

import com.store.cars.Exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

/**
 * Service which communicate with JPA Repository. It's retrieving an orders from RestController and creates operations on
 * the JPA repository.
 */

public class VehicleService implements IVehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @ModelAttribute
    @Override
    public List<Vehicle> getVehiclesList(Specification<Vehicle> vehicleSpecification) {
        return vehicleRepository.findAll(vehicleSpecification);
    }

    @Override
    public List<Vehicle> getVehiclesListByBrand(String brand) {
        return vehicleRepository.findAllByBrand(brand);
    }

    @Override
    public Vehicle findVehicleById(Integer vehicleId) throws ResourceNotFoundException {
        Vehicle vehicle = vehicleRepository
                            .findById(vehicleId)
                            .orElseThrow(() -> new ResourceNotFoundException("Vehicle not found on :: " + vehicleId));
        return vehicle;
    }

    @Override
    public void delete(Vehicle vehicle) {
        vehicleRepository.delete(vehicle);
    }

    @Override
    public Vehicle createVehicle(Vehicle vehicle) {
        return vehicleRepository.save(vehicle);
    }

    @Override
    public void updateVehicle(Vehicle vehicle) {
        vehicleRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> findAll(){
        return vehicleRepository.findAll();
    }
}
