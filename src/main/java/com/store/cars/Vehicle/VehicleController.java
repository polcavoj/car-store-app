package com.store.cars.Vehicle;

import com.store.cars.Exceptions.ResourceNotFoundException;
import com.store.cars.Filtering.*;
import com.store.cars.Vehicle.DTO.VehicleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


/**
 * REST API which is making a CRUD operations on the Vehicle Repository.
 */
@RestController
class VehicleController {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IVehicleService vehicleService;

    public VehicleController(VehicleRepository carStoreService, VehicleService vehicleService){
        this.vehicleRepository = carStoreService;
        this.vehicleService = vehicleService;
    }

    /**
     * GET Method which retrieve a specific vehicle from database with given ID.
     *
     * @param vehicleID ID of vehicle to search
     * @return VehicleDTO with every information except ID.
     * @throws ResourceNotFoundException
     */
    @GetMapping(value = "/vehicle/{id}")
    public VehicleDTO findVehicle(@PathVariable(value = "id") Integer vehicleID) throws ResourceNotFoundException{
        Vehicle vehicle = vehicleService.findVehicleById(vehicleID);
        return modelMapper.map(vehicle, VehicleDTO.class);
    }

    /**
     * GET method, which retrieve a list of all vehicles in the database. Method allows filtering through the parameters.
     *
     * @param category optional paramater used for filtering through the category attribute
     * @param brand optional paramater used for filtering through the brand attribute
     * @param model optional paramater used for filtering through the model attribute
     * @param price optional paramater used for filtering through the price attribute
     * @param date optional paramater used for filtering through the date attribute
     * @return
     */
    @GetMapping(value = "/vehicles")
    public List<VehicleDTO> findVehicles(@RequestParam(value = "category", required = false) String category,
                                         @RequestParam(value = "brand", required = false) String brand,
                                         @RequestParam(value = "model", required = false) String model,
                                         @RequestParam(value = "price", required = false) Integer price,
                                         @DateTimeFormat(pattern = "yyyy-MM-dd")
                                         @RequestParam(value = "commissioning", required = false) LocalDate date){

        Specification<Vehicle> vehicleSpecification = Specification.where(new  VehicleWithCategory(category)
                                                                                .and(new VehicleWithBrand(brand))
                                                                                .and(new VehicleWithModel(model))
                                                                                .and(new VehicleWithPrice(price))
                                                                                .and(new VehicleWithCommissioning(date)));
        List<Vehicle> vehicles;

        if( brand != null )
            vehicles = vehicleService.getVehiclesListByBrand(brand);
        else
            vehicles = vehicleService.getVehiclesList(vehicleSpecification);

        return vehicles.stream().map(vehicle -> modelMapper.map(vehicle, VehicleDTO.class))
                .collect(Collectors.toList());
    }

    /**
     *  POST method which takes data from client, put them in the VehicleDTO object and transform them to the Vehicle object,
     *  which can be saved to the database.
     *
     * @param vehicleDTO object which containts values from user.
     * @return message about creating
     */
    @PostMapping(value = "/vehicles")
    public String saveVehicle(@RequestBody VehicleDTO vehicleDTO){
        Vehicle vehicle = modelMapper.map(vehicleDTO, Vehicle.class);
        Vehicle newVehicle = vehicleService.createVehicle(vehicle);
        return "Saving of vehicle " + newVehicle.getId() + " succesfull.\n";
    }

    /**
     * PUT method which takes data from client, put them in the VehicleDTO object and transform the to the Vehicle object.
     * Then it will try to find a vehicle by given ID - it it not exists, Exception is thrown, else it will take a gear from
     * old vehicle and put it to the updated vehicle.
     *
     * @param vehicleID ID of vehicle to updated.
     * @param vehicleDTO object which containts values from user.
     * @return message about updating
     * @throws ResourceNotFoundException
     */
    @PutMapping(value = "/vehicle/{id}")
    public String updateVehicle(@PathVariable(value = "id") Integer vehicleID, @RequestBody VehicleDTO vehicleDTO) throws ResourceNotFoundException{
        Vehicle vehicle = modelMapper.map(vehicleDTO, Vehicle.class);
        vehicle.setId(vehicleID);
        Vehicle oldVehicle = vehicleService.findVehicleById(vehicleID);
        vehicle.setGears(oldVehicle.getGears());
        vehicleService.updateVehicle(vehicle);
        return "Update of vehicle " + vehicleID + " succesfull.\n";
    }

    /**
     * Delete method which find a record in the database (or throw an Exception if its not found) and delete it.
     *
     * @return String with information about deleting a record.
     * @param vehicleId id of the deleted vehicle
     */
    @DeleteMapping("/vehicle/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String deleteVehicle(@PathVariable(value = "id") Integer vehicleId) throws ResourceNotFoundException {
        Vehicle vehicle = vehicleService.findVehicleById(vehicleId);
        vehicleService.delete(vehicle);
        return "Vehicle " + vehicleId + " deleted.\n";
    }
}
