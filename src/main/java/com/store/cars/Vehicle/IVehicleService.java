package com.store.cars.Vehicle;

import com.store.cars.Exceptions.ResourceNotFoundException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.ui.Model;

import java.util.List;

/**
 * Interface which is responsible for communicating with the Vehicle JpaRepository.
 */

public interface IVehicleService {

    /**
     * Method which find vehicles by client specification defined in GET request.
     *
     * @param vehicleSpecification filter specification.
     * @return List of vehicles which complying the specification.
     */
    List<Vehicle> getVehiclesList(Specification<Vehicle> vehicleSpecification);

    /**
     * Special method for filtering with the attribute Brand. Method is single because the brand attribute is
     * in another class (db table).
     *
     * @param brand Name of the brand for filtering.
     * @return List of vehicles which complying the brand name filtering.
     */
    List<Vehicle> getVehiclesListByBrand(String brand);

    /**
     * Method which try to find a vehicle with given ID in the database. If it fails, Exception is thrown.
     *
     * @param vehicleId ID of vehicle to search
     * @return Vehicle from DB with given ID.
     * @throws ResourceNotFoundException
     */
    Vehicle findVehicleById(Integer vehicleId) throws ResourceNotFoundException;

    /**
     * Service method which call a basic method of JPARepository and delete given vehicle.
     *
     * @param vehicle Vehicle to delete.
     */
    void delete(Vehicle vehicle);

    /**
     * Method which call a basic save method of JPARepository and save given vehicle to the db.
     * @param vehicle Vehicle to save without ID - it will create a new vehicle.
     */
    Vehicle createVehicle(Vehicle vehicle);

    /**
     * Method which call a basic save method of JPARepository and save given vehicle to the db.
     * @param vehicle Vehicle to save with ID - it will rewrite an existing vehicle.
     */
    void updateVehicle(Vehicle vehicle);

    List<Vehicle> findAll();

}
