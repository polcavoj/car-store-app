package com.store.cars.Vehicle.DTO;

import com.store.cars.Gear.Gear;
import com.store.cars.System.LocalDateAttributeConverter;
import com.store.cars.Brand.Brand;
import org.springframework.core.style.ToStringCreator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Convert;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * Simple DTO object for Vehicle class. Its used when client adds car through the web site.
 */

public class NewVehicleDTO {

    @NotEmpty
    private String      category;

    @NotEmpty
    private String     brand;

    @NotEmpty
    private String      model;

    @NotEmpty
    private String      vehicleType;

    @NotNull
    @Min(1)
    private Integer     price;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate   commissioning;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate   soldAtDate;

    private List<Gear> gears;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public LocalDate getCommissioning() {
        return commissioning;
    }

    public void setCommissioning(LocalDate commissioning) {
        this.commissioning = commissioning;
    }

    public LocalDate getSoldAtDate() {
        return soldAtDate;
    }

    public void setSoldAtDate(LocalDate soldAtDate) {
        this.soldAtDate = soldAtDate;
    }

    public List<Gear> getGears() {
        return gears;
    }

    public void setGears(List<Gear> gears) {
        this.gears = gears;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("category ", this.getCategory())
                .append("brand ", this.getBrand())
                .append("model ", this.getModel())
                .append("type ", this.getVehicleType())
                .append("price ", this.getPrice())
                .append("commissioning ", this.getCommissioning())
                .append("soldAtDate ", this.getSoldAtDate()).toString();
    }
}
