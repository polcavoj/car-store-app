package com.store.cars.Vehicle;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Simple vehicle repository, which extends JPA Specification Executor for filtering requests from client.
 */

public interface VehicleRepository extends JpaRepository<Vehicle, Integer>, JpaSpecificationExecutor<Vehicle> {

    /**
     * Main find method, which search in the database based on the specification of the client.
     *
     * @param vehicleSpecification parameter for filtering in the database
     * @return List of vehicles which complying the specification.
     */
    List<Vehicle> findAll(Specification<Vehicle> vehicleSpecification);

    /**
     * Specific method for filtering with a brand attribute.
     *
     * @param name
     * @return List of vehicles which complying the filter from the client
     */
    @Query("SELECT DISTINCT vehicle FROM Vehicle vehicle LEFT JOIN vehicle.brand_id b WHERE b.name LIKE :name%")
    @Transactional(readOnly = true)
    List<Vehicle> findAllByBrand(@Param("name") String name);
}
