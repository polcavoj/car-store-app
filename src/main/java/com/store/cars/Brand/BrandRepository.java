package com.store.cars.Brand;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface BrandRepository extends JpaRepository<Brand, Integer> {

    @Query("SELECT DISTINCT brand FROM Brand brand WHERE brand.name LIKE :name%")
    @Transactional
    Brand findByName(@Param("name") String name);
}
