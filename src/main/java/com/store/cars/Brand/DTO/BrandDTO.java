package com.store.cars.Brand.DTO;

import javax.validation.constraints.NotNull;

/**
 * Simple DTO object for Brand class. It exposed Brand except of it's ID. *
 */
public class BrandDTO {

    @NotNull
    private String      name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
