package com.store.cars.Brand;

import java.util.List;

public interface IBrandService {

    List<Brand> getBrandList();

    Brand   findByName(String name);
}
