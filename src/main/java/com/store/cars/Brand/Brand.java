package com.store.cars.Brand;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * Simple Java class which represent Brand entity.
 */
@Entity
@Table(name = "brands")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer     id;

    @Column(name = "name")
    @NotEmpty
    private String      name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
