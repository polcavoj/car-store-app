package com.store.cars.Brand;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BrandService implements IBrandService{

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public List<Brand> getBrandList() {
        return brandRepository.findAll();
    }

    @Override
    public Brand findByName(String name) {
        return brandRepository.findByName(name);
    }
}
