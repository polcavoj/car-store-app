package com.store.cars.Filtering;

import com.store.cars.Vehicle.Vehicle;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Simple class which implements specification for filtering with category attribute.
 */

public class VehicleWithCategory implements Specification<Vehicle> {

    private String category;

    public VehicleWithCategory(String category){
        this.category = category;
    }

    @Override
    public Predicate toPredicate(Root<Vehicle> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (category == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true = no filtering
        }
        return criteriaBuilder.equal(root.get("category"), this.category);
    }
}
