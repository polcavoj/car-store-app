package com.store.cars.Filtering;

import com.store.cars.Vehicle.Vehicle;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Simple class which implements specification for filtering with Brand attribute.
 */
public class VehicleWithBrand implements Specification<Vehicle> {
    private String brand;

    public VehicleWithBrand(String brand){
        this.brand = brand;
    }

    @Override
    public Predicate toPredicate(Root<Vehicle> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (brand == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true = no filtering
        }
        return criteriaBuilder.equal(root.get("brand"), this.brand);
    }
}
