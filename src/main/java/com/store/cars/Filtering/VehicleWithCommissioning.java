package com.store.cars.Filtering;

import com.store.cars.Vehicle.Vehicle;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;


/**
 * Simple class which implements specification for filtering with Commissioning attribute.
 */
public class VehicleWithCommissioning implements Specification<Vehicle> {

    private LocalDate date;

    public VehicleWithCommissioning(LocalDate date){
        this.date = date;
    }

    @Override
    public Predicate toPredicate(Root<Vehicle> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (date == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true = no filtering
        }
        return criteriaBuilder.equal(root.get("commissioning"), this.date);
    }
}
