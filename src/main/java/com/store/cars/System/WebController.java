package com.store.cars.System;

import com.store.cars.Brand.Brand;
import com.store.cars.Brand.BrandRepository;
import com.store.cars.Brand.BrandService;
import com.store.cars.Brand.IBrandService;
import com.store.cars.Exceptions.ResourceNotFoundException;
import com.store.cars.Vehicle.DTO.NewVehicleDTO;
import com.store.cars.Vehicle.DTO.VehicleDTO;
import com.store.cars.Vehicle.IVehicleService;
import com.store.cars.Vehicle.Vehicle;
import com.store.cars.Vehicle.VehicleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.LocalDate;

/**
 * WebController which is responsible for maintenance of Web pages.
 */
@Controller
public class WebController {

    private String [] category = {"Automobil", "Motocykl", "Prives"};

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    private IBrandService brandService;

    @Autowired BrandRepository brandRepository;

    public WebController(BrandRepository brandRepository, BrandService brandService){
        this.brandRepository = brandRepository;
        this.brandService = brandService;
    }

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("vehicles", vehicleService.findAll());
        return "home";
    }

    @GetMapping("/addVehicle")
    public String addVehicle(Model model){
        model.addAttribute("vehicleToAdd", new NewVehicleDTO());
        model.addAttribute("category", category);
        model.addAttribute("brands", brandService.getBrandList());
        return "addVehicle";
    }

    @PostMapping("/addVehicle")
    public String addVehicle(@ModelAttribute("vehicleToAdd") @Valid NewVehicleDTO vehicleToAdd, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            model.addAttribute("category", category);
            model.addAttribute("brands", brandService.getBrandList());
            return "addVehicle";
        }
        Brand brand = brandService.findByName(vehicleToAdd.getBrand());
        Vehicle newVehicle = modelMapper.map(vehicleToAdd, Vehicle.class);
        newVehicle.setBrand_id(brand);
        vehicleService.createVehicle(newVehicle);

        model.addAttribute("vehicles", vehicleService.findAll());
        return "home";
    }

    @PostMapping("/sellVehicle")
    public String sellVehicle(@ModelAttribute Vehicle vehicle, Model model) throws ResourceNotFoundException {
        Vehicle vehicleToSell = vehicleService.findVehicleById(vehicle.getId());
        vehicleToSell.setSoldAtDate(LocalDate.now());
        vehicleService.updateVehicle(vehicleToSell);

        model.addAttribute("vehicles", vehicleService.findAll());
        return "home";
    }

}
