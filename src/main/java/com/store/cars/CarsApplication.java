package com.store.cars;

import com.store.cars.Brand.BrandService;
import com.store.cars.Vehicle.IVehicleService;
import com.store.cars.Vehicle.VehicleService;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}

	@Bean
	public VehicleService vehicleService(){
		return new VehicleService();
	}

	@Bean
	public BrandService brandService(){
		return new BrandService();
	}
}
