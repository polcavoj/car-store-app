package com.store.cars.Gear.DTO;

import javax.validation.constraints.NotNull;

public class GearDTO {

    @NotNull
    private String      name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
