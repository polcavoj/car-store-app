package com.store.cars.Gear;

import org.springframework.data.repository.Repository;

public interface GearRepository extends Repository<Gear, Integer> {
}
