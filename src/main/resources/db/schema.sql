DROP TABLE vehicles IF EXISTS;
DROP TABLE gear IF EXISTS;

CREATE TABLE brands (
    id          INTEGER IDENTITY PRIMARY KEY,
    name        VARCHAR(30)
);

CREATE TABLE vehicles (
    id            INTEGER IDENTITY PRIMARY KEY,
    category      VARCHAR(10) DEFAULT 'Automobil',
    brand_id      INTEGER NOT NULL,
    FOREIGN KEY (brand_id) REFERENCES brands(id),
    model         VARCHAR(30),
    vehicle_type  VARCHAR(20),
    price         INTEGER,
    commissioning DATE,
    sold_at_date  DATE
);

ALTER TABLE vehicles
    ADD CONSTRAINT CK_exampleTable_newColumn CHECK (category IN ('Automobil','Motocykl','Prives'));

CREATE TABLE gear (
    id            INTEGER IDENTITY PRIMARY KEY,
    name          varchar(50)
);

CREATE TABLE gear_in_vehicles (
    vehicle_id    INTEGER NOT NULL,
    gear_id       INTEGER NOT NULL,
    FOREIGN KEY (vehicle_id) REFERENCES vehicles(id),
    FOREIGN KEY (gear_id) REFERENCES gear(id)
);