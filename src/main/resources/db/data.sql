INSERT INTO brands ( name ) VALUES ( 'Skoda' );
INSERT INTO brands ( name ) VALUES ( 'Dacia' );
INSERT INTO brands ( name ) VALUES ( 'Suzuki' );
INSERT INTO brands ( name ) VALUES ( 'Yamaha' );
INSERT INTO brands ( name ) VALUES ( 'Honda' );
INSERT INTO brands ( name ) VALUES ( 'Handy' );
INSERT INTO brands ( name ) VALUES ( 'Nitram' );
INSERT INTO brands ( name ) VALUES ( 'Ford' );
INSERT INTO brands ( name ) VALUES ( 'Renault' );
INSERT INTO brands ( name ) VALUES ( 'Peugeot' );

INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Superb', 'Laurin a Klement', 1000000, '2019-05-03' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Octavia', 'Laurin a Klement', 440000, '2015-07-10' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Octavia', 'Laurin a Klement', 190000, '2013-11-14' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Kodiaq', 'Laurin a Klement', 880900, '2019-02-15' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Fabia', 'Active', 269900, '2018-07-14' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Fabia', 'Monte Carlo', 400900, '2019-06-11' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Scala', 'Style', 449900, '2018-11-12' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 0, 'Kamiq', 'Style', 437900, '2018-10-17' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 1, 'Logan', 'Arctica', 211900, '2018-07-01' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 1, 'Sandero', 'Techroad', 298900, '2018-02-10' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 1, 'Sandero', 'Stepaway', 280900, '2018-07-13' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 1, 'Duster', 'Prestige', 361900, '2019-03-03' )
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Automobil', 2, 'Vitara', 'Elegance', 501900, '2019-04-17' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Motocykl', 2, 'Address', 'Scooter', 54900, '2019-04-17' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Motocykl', 2, 'V-Strom', 'Enduro', 209900, '2019-03-20' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Motocykl', 3, 'XSR700', 'Sport Heritage', 194900, '2018-06-10' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Motocykl', 3, 'Tenere', 'Adventure', 249900, '2018-07-31' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Motocykl', 4, 'VFR800X', 'Adventure', 329900, '2018-06-15' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Prives', 5, 'HANDY-8', 'Handy', 13070, '2019-05-31' );
INSERT INTO vehicles ( category, brand_id, model, vehicle_type, price, commissioning ) VALUES ( 'Prives', 6, 'Nitram-4', 'Nitram', 88000, '2019-06-15' );

INSERT INTO gear ( name ) VALUES ( 'ABS' );
INSERT INTO gear ( name ) VALUES ( 'ESP' );
INSERT INTO gear ( name ) VALUES ( 'EBD' );
INSERT INTO gear ( name ) VALUES ( 'bixenonové světlomety' );
INSERT INTO gear ( name ) VALUES ( 'střešní okno' );
INSERT INTO gear ( name ) VALUES ( 'adaptivní tempomat' );
INSERT INTO gear ( name ) VALUES ( 'adaptivní podvozek' );
INSERT INTO gear ( name ) VALUES ( 'vyhřívané čelní sklo' );
INSERT INTO gear ( name ) VALUES ( 'parkovací asistent' );
INSERT INTO gear ( name ) VALUES ( 'vyhřívání předních sedadel' );
INSERT INTO gear ( name ) VALUES ( 'vyhřívání zadních sedadel' );
INSERT INTO gear ( name ) VALUES ( 'elektrická dětská pojistka' );
INSERT INTO gear ( name ) VALUES ( 'světelný asistent' );
INSERT INTO gear ( name ) VALUES ( 'KESSY' );
INSERT INTO gear ( name ) VALUES ( 'signalizace vzdálenosti při parkování' );
INSERT INTO gear ( name ) VALUES ( 'stínící rola' );
INSERT INTO gear ( name ) VALUES ( 'osvětlení prostoru pro nohy' );
INSERT INTO gear ( name ) VALUES ( 'střešní nosič' );
INSERT INTO gear ( name ) VALUES ( 'Sound System Canton s 10 reproduktory' );
INSERT INTO gear ( name ) VALUES ( 'elektricky nastavitelná sedadla' );
INSERT INTO gear ( name ) VALUES ( 'kola z lehké slitiny' );
INSERT INTO gear ( name ) VALUES ( 'virtuální pedál' );

INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 4 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 5 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 7 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 12 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 16 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 0, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 5 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 7 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 12 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 16 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 1, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 5 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 7 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 12 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 16 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 2, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 4 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 5 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 7 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 12 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 16 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 4, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 5, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 5, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 5, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 5, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 5, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 5, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 4 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 5 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 6, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 7, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 7, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 7, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 7, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 7, 13 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 7, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 8, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 8, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 8, 6 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 8, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 8, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 8, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 9, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 9, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 9, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 9, 4 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 9, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 9, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 10, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 10, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 10, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 10, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 10, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 10, 18 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 2 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 5 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 8 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 9 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 12 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 16 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 11, 17 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 12, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 12, 1 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 12, 10 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 12, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 12, 15 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 12, 20 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 13, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 13, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 13, 19 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 14, 0 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 14, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 14, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 14, 19 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 15, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 15, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 15, 19 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 16, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 16, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 17, 3 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 17, 11 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 17, 19 );
INSERT INTO gear_in_vehicles( vehicle_id, gear_id ) VALUES ( 19, 0 );