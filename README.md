### Car store Spring app

Simple application for car sale with REST API and web frontend implemented in Thymeleaf. It's a spring-boot application built using Maven. There are two ways to run this application, one with building a jar file and run it from command line:
```
git clone git@gitlab.com:polcavoj/car-store-app.git
cd car-store-app
./mvnw package
java -jar target/*.jar
```
Second way to run an application is directly from Maven using the Spring Boot Maven plugin.
```
git clone git@gitlab.com:polcavoj/car-store-app.git
cd car-store-app
mvn spring-boot:run
```
Access to the application will be here: http://localhost:8080/.

Simple curl commands for testing the API.

##### GET
- Info about one vehicle: <i>/vehicle/{id}</i>
- A list of all vehicles to sale: <i>/vehicles</i>
- A list of all vehicles to sale with filter: <i>/vehicles?{category,brand,model,price,commissioning}={value}</i>
```
curl localhost:8080/vehicle/0
curl localhost:8080/vehicles
curl localhost:8080/vehicles?category=Automobil
curl localhost:8080/vehicles?brand=Dacia
```

##### POST

- Save a new vehicle to the database. All attributes are mandatory. Mapping on url: <i>/vehicles</i>
```
curl -d "{\"category\":\"Automobil\",\"brand_id\":{\"id\":0,\"name\":\"Skoda\"},\"model\":\"Superb\",\"vehicleType\":\"Laurin a Klement\",\"price\":1000000,\"commissioning\":\"2019-05-03\"}" -H "Content-type: application/json" -X POST localhost:8080/vehicles
```

##### PUT
- Update a vehicle in the database. All attributes are mandatory. Mapping on url: <i>/vehicle/{id}</i>
```
curl -d "{\"category\":\"Automobil\",\"brand_id\":{\"id\":0,\"name\":\"Skoda\"},\"model\":\"Superb\",\"vehicleType\":\"Laurin a Klement\",\"price\":1000000,\"commissioning\":\"2019-05-03\"}" -H "Content-type: application/json" -X PUT localhost:8080/vehicle/0
```

##### DELETE
- Delete a vehicle from database by given ID: <i>/vehicle/{id}</i>
```
curl -X DELETE localhost:8080/vehicle/0
```